---
bookCollapseSection: true
weight: 2
---
# About this Release

## What's new in this release

[Entgra IoT Server](https://entgra.io/) version 4.0.0 is the successor of [Entgra IoT Server 3.8.0](https://entgra-documentation.gitlab.io/v3.8.0/). Some prominent features and enhancements are
 as follows:
