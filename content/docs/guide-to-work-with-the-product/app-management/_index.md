---
bookCollapseSection: true
weight: 9
---

# Managing Applications
Application management section takes you through on How to Publish an App, manage its Lifecycle and new releases. There is also a section on Google Enterprise App Management guiding you through on how to enroll a device as Google enterprise enabled work profile. 

How to Install and Uninstall an App is also covered within this section. 
