---
bookCollapseSection: true
weight: 8
---

# Managing Roles

Entgra IoTS is shipped with a set of default roles. However, if required, tenant administrators will be able to create new customized roles. Tenant administrators can use roles to manage the users and their devices. While end-users allocated with device operation permissions can manage their own devices via the Entgra IoTS Console. Administrators can create roles, assign them to a user or a group of users, and edit or delete existing roles.

## Adding a role and permissions 


Follow the instructions below to add a role:

1.  [Sign in to the Entgra IoT Server console]({{< param doclink >}}guide-to-work-with-the-product/login-guide/).

    If you want to try out Entgra IoT Server as an admin, use **admin** as the username and the password.

2.  You can navigate to the **ADD ROLE** page via the following methods: 

    1.  Method 01: 
        
        Click **menu** icon 
    
        <img src = "../../image/4000.png" style="border:5px solid black "> 
    
        Select **USER MANAGEMENT ** 
    
        <img src = "../../image/12345.png" style="border:5px solid black">
        
        Select **ROLES**
        
        <img src = "../../image/352820159.png" style="border:5px solid black">
        
        Select **ADD ROLE** button.  
        
        <img src = "../../image/1111.png" style="border:5px solid black">
        
    2.  Method 02: Click **Add** icon on the **ROLES** tile. 
    
        <img src = "../../image/600058.png" style="border:5px solid black"> 

3.  Provide the required details and click **Add Role**.
    *   **Domain**: Provide the user store type from the list of items.
    *   **Role Name**: Provide the role name. 

    *   **User Lis**t: Define the users belonging to the respective role. Type the first few 
    characters of the username and Entgra IoT Server will provide the list of users having the 
    same characters. You can then select the user/s you wish to add.
    
    <img src = "../../image/352820286.png" style="border:5px solid black">

4.  Define the permissions that need to be associated with the role you created by selecting the permissions from the permission tree. As the permissions are categorized, when the main permission category is selected, all its sub-permissions will get selected automatically. 

    Make sure to select the **Login** permission. Without this permission, the users are unable to log in to Entgra IoT Server.

    For more information on the APIs associated with the permissions, see [Permission APIs]({{< param doclink >}}using-entgra-iot-server/product-administration/user-management/#configuring-role-permissions).

<table>
   <colgroup>
      <col>
      <col>
   </colgroup>
   <tbody>
      <tr>
         <th>Permissions</th>
         <th>Description</th>
      </tr>
      <tr>
         <td>
            <p><strong>Applications management</strong>
               <br><span class="confluence-embedded-file-wrapper"><img class="confluence-embedded-image" src="352820204.png" data-image-src="attachments/352820123/352820204.png" data-unresolved-comment-count="0" data-linked-resource-id="352820204" data-linked-resource-version="1" data-linked-resource-type="attachment" data-linked-resource-default-alias="Manage-App-Permissions.png" data-base-url="https://entgra.atlassian.net/wiki" data-linked-resource-content-type="image/png" data-linked-resource-container-id="352820123" data-linked-resource-container-version="1" data-media-id="6d7487af-c07a-41a7-85a8-1d290cf2e0e5" data-media-type="file"></span>
            </p>
         </td>
         <td>
            <p>You can install applications on devices registered with Entgra IoT Server via the App Store or you can install applications via the internal REST APIs that is available on Entgra IoT Server. This permission ensures that a user is able to install and uninstall applications via the internal APIs that are available in Entgra IoT Server.</p>
            <p>For more information on installing applications via the App Store, see <a href="{{< param doclink >}}guide-to-work-with-the-product/app-management/browser-and-install-app/">Installing Mobile Apps</a>.</p>
         </td>
      </tr>
      <tr>
         <td>
            <p><strong>Certificate management</strong></p>
            <p><strong><span class="confluence-embedded-file-wrapper confluence-embedded-manual-size"><img class="confluence-embedded-image" width="250" src="352820142.png?width=250" data-image-src="attachments/352820123/352820142.png" data-unresolved-comment-count="0" data-linked-resource-id="352820142" data-linked-resource-version="1" data-linked-resource-type="attachment" data-linked-resource-default-alias="RolePermissions_CertificateManagement.png" data-base-url="https://entgra.atlassian.net/wiki" data-linked-resource-content-type="image/png" data-linked-resource-container-id="352820123" data-linked-resource-container-version="1" data-media-id="67a5dfe5-3dfc-4733-b0c2-794e17eeee61" data-media-type="file"></span><br></strong></p>
         </td>
         <td>
            <p>Entgra IoT Server supports mutual SSL, where the client verifies that the server can be trusted and the server verifies that the client can be trusted by using digital signatures. Following permissions grant access to client-side mutual SSL certificates:</p>
            <ul>
               <li><strong>device-mgt &gt; certificates &gt; manage</strong>: This permission enables to create certificates and access own certificates.</li>
               <li>
                  <strong>device-mgt &gt; admin &gt; certificates</strong>: These permissions ensure that a user is able to access all available certificates. Users with these permissions can:
                  <ul>
                     <li>View all certificates in a list view and in a detailed view</li>
                     <li>Create and remove certificates</li>
                     <li>Verify certificates: This allows an authorized user to authenticate and authorize a device by implementing on-behalf-of authentication.</li>
                  </ul>
               </li>
            </ul>
         </td>
      </tr>
      <tr>
         <td>
         </td>
         <td>
            <p>The monitoring frequency is configured under the general platform configurations in Entgra IoT Server. The IoT server uses this parameter to determine how often the devices enrolled with Entgra IoT Server need to be monitored.</p>
            <p>This permission enables users to configure, update and view the general platform configurations in Entgra IoT Server. In the general platform configurations, you need to define the monitoring frequent, which is how often the IoT server communicates with the device agent.</p>
            <p>For more information, see <a href="{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/general-platform-configurations/">General Platform Configurations</a>.</p>
         </td>
      </tr>
      <tr>
         <td>
            <p><strong>Manage devices</strong></p>
            <p><span class="confluence-embedded-file-wrapper confluence-embedded-manual-size"><img class="confluence-embedded-image" width="310" src="352820165.png?width=310" data-image-src="attachments/352820123/352820165.png" data-unresolved-comment-count="0" data-linked-resource-id="352820165" data-linked-resource-version="1" data-linked-resource-type="attachment" data-linked-resource-default-alias="RolePermissions_DeviceManagement.png" data-base-url="https://entgra.atlassian.net/wiki" data-linked-resource-content-type="image/png" data-linked-resource-container-id="352820123" data-linked-resource-container-version="1" data-media-id="3b31d63b-6ef4-48a2-b7cf-0b4257862381" data-media-type="file"></span></p>
         </td>
         <td>
            <ul>
               <li><strong>device-mgt &gt; any-device &gt; permitted-actions-under-owning-device</strong>: This permission enables you to view and manage all the devices shared with you.</li>
               <li>
                  <strong>device-mgt &gt; devices &gt; owning-device</strong>: These permissions enable users to:
                  <ul>
                     <li>Enroll and disenroll devices</li>
                     <li>Publish events received by the device client, to the analytics profile</li>
                     <li>Setup geofencing alerts</li>
                     <li>Modify device details such as name and description</li>
                     <li>Retrieve analytics for devices</li>
                  </ul>
               </li>
            </ul>
         </td>
      </tr>
      <tr>
         <td>
         </td>
         <td>This permission enables you to disenroll or unregister Android and Windows devices.</td>
      </tr>
      <tr>
         <td>
         </td>
         <td>This permission enables you to enroll or register Android, iOS and Windows devices with Entgra IoT Server.</td>
      </tr>
      <tr>
         <td>
            <p><strong>Device status</strong></p>
            <p><strong><span class="confluence-embedded-file-wrapper confluence-embedded-manual-size"><img class="confluence-embedded-image confluence-thumbnail" width="190" src="352820183.png?width=190" data-image-src="attachments/352820123/352820183.png" data-unresolved-comment-count="0" data-linked-resource-id="352820183" data-linked-resource-version="1" data-linked-resource-type="attachment" data-linked-resource-default-alias="RolePermissions_ChangeDeviceStatus.png" data-base-url="https://entgra.atlassian.net/wiki" data-linked-resource-content-type="image/png" data-linked-resource-container-id="352820123" data-linked-resource-container-version="1" data-media-id="8fc809a3-4006-43ad-9c1e-b0efc27a89d5" data-media-type="file"></span><br></strong></p>
         </td>
         <td>This permission enables you to change a device status.</td>
      </tr>
      <tr>
         <td>
         </td>
         <td>Entgra IoT Server offers various device operations based on the mobile platform. This permission enables users to view and carry out device operations on their devices. Expand the preferred platform and select the operations that need to be enabled for users that belong to the role&nbsp;you are creating.</td>
      </tr>
      <tr>
         <td>
         </td>
         <td>
            <p>In Entgra IoT Server the settings can be customized for each platform. This permission enables you to maintain and customize the notification type, notification frequency, and the End User License Agreement (EULA) to suit the requirement of Android, iOS, and Windows mobile platform.</p>
            <p>For more information, see <a href="{{< param doclink >}}guide-to-work-with-the-product/enrollment-guide/enroll-android/android-configurations/" data-linked-resource-id="352822931" data-linked-resource-version="1" data-linked-resource-type="page">Android platform settings</a>, <a href="{{< param doclink >}}guide-to-work-with-the-product/enrollment-guide/enroll-ios/additional-server-configs-for-apple-devices/apple-server-configurations/#add-platform-configurations" data-linked-resource-id="352824279" data-linked-resource-version="1" data-linked-resource-type="page">iOS platform settings</a> and <a href="{{< param doclink >}}guide-to-work-with-the-product/enrollment-guide/enroll-windows/additional-server-configs-for-windows/" data-linked-resource-id="352824790" data-linked-resource-version="1" data-linked-resource-type="page">Windows platform settings</a>.</p>
         </td>
      </tr>
      <tr>
         <td>
         </td>
         <td>
            <p>The failure to carry out operations will be notified to the Entgra IoT Server administrator and the device owner. This permission enables you to view the notifications that were sent.</p>
         </td>
      </tr>
      <tr>
         <td>
         </td>
         <td>
            <p>In Entgra IoT Server, you can define policies, which include a set of configurations. Entgra IoT Server policies are enforced on the Entgra IoT Server users' devices when new users register with the Entgra IoT Server. The Entgra IoT Server&nbsp;policy settings will vary based on the mobile OS type.</p>
            <p>This permission enables you to add, modify, view, publish, unpublish and remove policies.</p>
            <p>For more information on working with policies, see Managing Policies.</p>
         </td>
      </tr>
      <tr>
         <td>
         </td>
         <td>
            <p>Entgra IoT Server allows you to create new customized roles.&nbsp;This permission enables you to add, modify, view and remove roles.</p>
            <p>For more information on working with roles, see <a href="{{< param doclink >}}guide-to-work-with-the-product/manage-roles/" data-linked-resource-id="352820118" data-linked-resource-version="1" data-linked-resource-type="page">Managing Roles</a>.</p>
         </td>
      </tr>
      <tr>
         <td>
         </td>
         <td>
            <p>Entgra IoT Server allows you to create and manage users. This permission enables you to add, modify, view and remove users.</p>
            <p>For more information on working with users, see <a href="{{< param doclink >}}/guide-to-work-with-the-product/manage-users/" data-linked-resource-id="352820590" data-linked-resource-version="1" data-linked-resource-type="page">Managing Users</a>.</p>
         </td>
      </tr>
      <tr>
         <td>
            <p><strong>Manage groups</strong></p>
            <p><strong><span class="confluence-embedded-file-wrapper confluence-embedded-manual-size"><img class="confluence-embedded-image" width="220" src="352820171.png?width=220" data-image-src="attachments/352820123/352820171.png" data-unresolved-comment-count="0" data-linked-resource-id="352820171" data-linked-resource-version="1" data-linked-resource-type="attachment" data-linked-resource-default-alias="RolePermissions_GroupManagement.png" data-base-url="https://entgra.atlassian.net/wiki" data-linked-resource-content-type="image/png" data-linked-resource-container-id="352820123" data-linked-resource-container-version="1" data-media-id="e2db1705-d224-4640-97f6-52e8c226ed4b" data-media-type="file"></span><br></strong></p>
         </td>
         <td>
            <p>These permissions enable you to manage groups pertaining to devices and user roles. The user role related permission enables viewing all user roles available in Entgra IoT Server. The device related permissions enable you to:</p>
            <ul>
               <li>Create and remove device groups</li>
               <li>Assign devices to a group</li>
               <li>Remove devices from a group</li>
               <li>View the list of groups attached to a device</li>
               <li>View the list of roles that have access to a group</li>
               <li>View the groups accessible by the logged in user</li>
            </ul>
         </td>
      </tr>
      <tr>
         <td>
         </td>
         <td>
            <p>You are able to create mobile apps in the App Publisher that is available in Entgra IoT Server. In order to create, publish, delete, install and update mobile applications the required permissions must be selected.</p>
            <p>To enable users to subscribe to applications and install an application on a device via the App Store you need to select Subscribe that is under the Web App permissions.</p>
            <p>For more information see the sections given below:</p>
            <ul>
               <li><a href="{{< param doclink >}}tutorials/mobile-application-management/" data-linked-resource-id="352813326" data-linked-resource-version="1" data-linked-resource-type="page">Creating Mobile Applications.</a></li>
               <li><a href="{{< param doclink >}}tutorials/mobile-application-management/installing-an-application-on-a-device/" data-linked-resource-id="352813484" data-linked-resource-version="1" data-linked-resource-type="page">Installing Mobile Apps</a>.</li>
            </ul>
         </td>
      </tr>
      <tr>
         <td>
            <p><strong>Device type management</strong></p>
            <p><strong><span class="confluence-embedded-file-wrapper confluence-embedded-manual-size"><img class="confluence-embedded-image" width="270" src="352820148.png?width=270" data-image-src="attachments/352820123/352820148.png" data-unresolved-comment-count="0" data-linked-resource-id="352820148" data-linked-resource-version="1" data-linked-resource-type="attachment" data-linked-resource-default-alias="RolePermissions_DeviceTypeManagement.png" data-base-url="https://entgra.atlassian.net/wiki" data-linked-resource-content-type="image/png" data-linked-resource-container-id="352820123" data-linked-resource-container-version="1" data-media-id="b1475769-f8cd-44f7-be1c-324180b449b0" data-media-type="file"></span><br></strong></p>
         </td>
         <td>
            <p>Following permissions enable managing device types:</p>
            <ul>
               <li><strong>device-mgt &gt; device-type &gt; add</strong>: This enables the ability to add or delete event definitions for device types.</li>
               <li><strong>device-mgt &gt; devicetype &gt; deploy</strong>: This enables deploying device type components via API. It is recommended to grant this permission to device admin users.</li>
            </ul>
         </td>
      </tr>
      <tr>
         <td>
            <p><strong>Authorization management</strong></p>
            <p><strong><span class="confluence-embedded-file-wrapper confluence-embedded-manual-size"><img class="confluence-embedded-image confluence-thumbnail" width="300" src="352820177.png?width=300" data-image-src="attachments/352820123/352820177.png" data-unresolved-comment-count="0" data-linked-resource-id="352820177" data-linked-resource-version="1" data-linked-resource-type="attachment" data-linked-resource-default-alias="RolePermissions_AuthorizationManagement.png" data-base-url="https://entgra.atlassian.net/wiki" data-linked-resource-content-type="image/png" data-linked-resource-container-id="352820123" data-linked-resource-container-version="1" data-media-id="5b8f6bad-9ae6-416d-a85f-53351b6fdaca" data-media-type="file"></span></strong></p>
         </td>
         <td>Users with this permission can check whether a user has the permission to access and manage a device. It is recommended to grant this permission to device admin users.</td>
      </tr>
   </tbody>
</table>

5.  Click **Update Role Permission**.


## Configuring Role Permissions



This section provides details on how to configure permissions by defining permissions to an API and the permissions associated with the APIs.

### Defining permissions for APIs

If you wish to create additional permission, follow the steps given below:

1.  Navigate to the JAX-RS web application that of your device types API folder. For more information, see the [permission XML file of the virtual fire-alarm.](https://github.com/wso2/carbon-device-mgt-plugins/blob/v4.0.55/components/device-types/virtual-fire-alarm-plugin/org.wso2.carbon.device.mgt.iot.virtualfirealarm.api/src/main/webapp/META-INF/permissions.xml)
2.  Define the new permission using the `@permission` annotation.  
    The `scope` defines to whom the API is limited to and the `permission` that is associated with a given API.  
    Example:

    `@Permission(scope = "virtual_firealarm_user", permissions = {"/permission/admin/device-mgt/user/operations"})`

3.  Restart Entgra IoT Server and you will see the new permission created in the permission tree.  
    Now only users who have this specific permission assigned to them will be able to control the buzzer of the fire-alarm.

### Permission APIs

Let's take a look at the default permissions associated with the APIs.


## Default Roles and Permissions


By default, Entgra IoTS includes a set of roles. These default roles and permissions have been explained in the following subsections.

### Default user roles

The following roles are available by default in Entgra IoTS:

*   **admin **- Role assigned to the super tenant administrator by default.

    If you are defining the permissions for an IoTS administrator who needs to perform operations and configure policies, make sure to select **admin**. The **admin** permission allows the user to perform operations and configure policies for devices.

     If you wish to create a user with administrative permission other than the default administrator in Entgra IoTS, follow the steps given below:

    1.  [Add a new a role](#adding-a-role-and-permissions).
    2.  Configure role permissions by specifically selecting the **admin** permission.

*   **internal-devicemgt-user** - This is a system reserved role with the minimum set of permissions to carry out operations. When a user creates an account before accessing the device management console the user is assigned the internal-device-mgt role by default.

### Permissions associated with user roles

<table>
  <tbody>
    <tr>
      <th>User role</th>
      <th>Allows Actions</th>
    </tr>
    <tr>
      <td>admin</td>
      <td>The super tenant administrator belongs to this role. By default, a super tenant administrator will have full control on all the device management consoles.</td>
    </tr>
    <tr>
      <td>devicemgt-user</td>
      <td>
        <p>Carryout external operations on a device based on the permissions assigned via the permission tree.</p>
        <p>Example: getting device details, registering a device control the buzzer and many more.</p>
      </td>
    </tr>
  </tbody>
</table>

## Removing a Role


Follow the instructions below to update a role:

1.  [Sign in to the IoTS device management console]({{< param doclink >}}guide-to-work-with-the-product/login-guide/) and click the menu icon.

    <img src = "../../image/352820436.png" style="border:5px solid black ">
    
2.  Click **User Management**.

    <img src = "../../image/20202020.png" style="border:5px solid black ">
    
3.  Click **Role**.  

    <img src = "../../image/30303030.png" style="border:5px solid black ">
    
4.  Click **Remove** on the role you wish to remove. 
 
    <img src = "../../image/40404040.png" style="border:5px solid black ">
    
    Click **REMOVE** to confirm that you want to remove the role. 
     
    <img src = "../../image/352820457.png" style="border:5px solid black ">



## Searching, Filtering and Soring Roles


### Searching for users

Follow the instructions given below to search for roles:

1.  [Sign in to the IoTS device management console]({{< param doclink >}}guide-to-work-with-the-product/login-guide/) and click the menu icon.

    <img src = "../../image/352820436.png" style="border:5px solid black ">
    
2.  Click **User Management**.

    <img src = "../../image/20202020.png" style="border:5px solid black ">
    
3.  Click **Role**.  

    <img src = "../../image/30303030.png" style="border:5px solid black ">
    
4.  Search for roles using the search bar.  

    <img src = "../../image/50505050.png" style="border:5px solid black ">

### Filtering users

Follow the instructions below to filter roles:

1.  [Sign in to the IoTS device management console]({{< param doclink >}}guide-to-work-with-the-product/login-guide/) and click the menu icon.

    <img src = "../../image/352820436.png" style="border:5px solid black ">
    
2.  Click **User Management**.

    <img src = "../../image/20202020.png" style="border:5px solid black ">
    
3.  Click **Role**.  

    <img src = "../../image/30303030.png" style="border:5px solid black ">
    
4.  Filter the roles by the role name.  

    <img src = "../../image/50505050.png" style="border:5px solid black ">

## Updating a role 


Follow the instructions below to update a role:

1.  [Sign in to the IoTS device management console]({{< param doclink >}}guide-to-work-with-the-product/login-guide/)Sign in 
to the IoTS device management console) and click the menu icon.

    <img src = "../../image/352820436.png" style="border:5px solid black ">
    
2.  Click **User Management**.

    <img src = "../../image/20202020.png" style="border:5px solid black ">
    
3.  Click **Role**.  

    <img src = "../../image/30303030.png" style="border:5px solid black ">
    
4.  Click **Edit** on the role you wish to update.  

    <img src = "../../image/23232323.png" style="border:5px solid black ">
    
5.  Update the required filed and click Update Role.  

    *   **Domain**: Provide the user store type from the list of items.
    *   **Role Name**: Provide the role name.

        <img src = "../../image/10101010.png" style="border:5px solid black ">



## Updating Role Permissions


Follow the instructions below to configure the role permissions:

1.  [Sign in to the IoTS device management console]({{< param doclink >}}guide-to-work-with-the-product/login-guide/) and click the menu icon.

    <img src = "../../image/352820436.png" style="border:5px solid black ">
    
2.  Click **User Management**.

    <img src = "../../image/20202020.png" style="border:5px solid black ">
    
3.  Click **Role**.  

    <img src = "../../image/30303030.png" style="border:5px solid black ">
    
4.  Click **Edit Permissions** on the role you wish to configure.  

    <img src = "../../image/222222.png" style="border:5px solid black ">

5.  Select or remove the permissions as required.   
    As the permissions are categorized, when the main permission category is selected, all its sub-permissions will get selected automatically. 

    If you are defining the permission for an administrator, make sure to select **admin**.

    For more information on the APIs associated with the permissions, see [Permission APIs]({{< param doclink>}}guide-to-work-with-the-product/manage-roles/#permission-apis). 

    <img src = "../../image/352820567.png" style="border:5px solid black ">

5.  click **Update Role Permissions**.
