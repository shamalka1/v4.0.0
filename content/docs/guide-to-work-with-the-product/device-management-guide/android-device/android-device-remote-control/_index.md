---
bookCollapseSection: true
weight: 1
---
# Remote Control for Android Devices

The remote control feature allows administrators to troubleshoot devices that are enrolled with Entgra IoT Server using the device management console. You can create a remote session, send adb shell commands, view the device logs, and remotely view and interact with the screen of an Android device.

Let's take a look at how you can start using it.

Before you begin!

*   Make sure to enroll an Android device. For more information, see [Android Device](https://entgra.atlassian.net/wiki/spaces/IoTS360/pages/352781661/Android+Device).
*   Make sure that your Android device supports the Android Lollipop (API level 21) version or above to use the [screen sharing](about:blank#RemoteControlforAndroidDevices-Viewthedevicescreen) feature.
*   The enrolled device needs to be connected to the network at all times.

## Create a remote session

Follow the steps given below to create a remote session between the device and Entgra IoT Server:

1.  Start Entgra IoT Server's core profile, which corresponds to the WSO2 Connected Device Management Framework (WSO2 CDMF) profile. 

2.  Access the device management console: `https://<IOTS_HTTPS_HOST>:9443/devicemgt` For example: `https://localhost:9443/devicemgt`
3.  Sign in as an administrator. By default, the username is `admin` and the password is `admin`.
4.  Under **DEVICES**, click **View**.  
    <img src="../../../../image/remote_session_01.png" style="border:5px solid black" >
     
    A page appears that lists all the devices that are enrolled with Entgra IoT Server because you have administrator privileges. If you do not have administrator privileges, you only the see the devices that you enrolled.
5.  Click the device you want to troubleshoot.
6.  Click the **Remote Session** tab.  
    <img src="../../../../image/remote_session_02.png" style="border:5px solid black" >
7.  Click **Connect to Device** to start a remote session with the device.   
    After the server connects to the device, you see the following screen:  
    <img src="../../../../image/remote_session_03.png" style="border:5px solid black" >
8.  You can now troubleshoot the device as described in the next sections. To stop the remote sharing session, click **Close Session.**

## Send adb shell commands

[Android Debug Bridge (adb)](http://adbshell.com/) is a command line tool that lets you communicate with an emulator or connected Android device. Follow the steps given below to troubleshoot the device using adb shell commands:

1.  If you haven't already [set up a remote session](about:blank#RemoteControlforAndroidDevices-Createaremotesession), set it up now.
2.  Click **Shell**.
3.  Write the shell command (see [adb shell commands](http://adbshell.com/commands/adb-shell-pwd) for the available commands) and press **Enter**.  
    For example, if you want to get the CPU and memory usage of the device, use the `top` adb command.  
    Sample output:  
    <img src="../../../../image/remote_session_04.png" style="border:5px solid black" >

## View device logs

[Logcat](https://developer.android.com/studio/command-line/logcat.html) is a tool that displays messages from the device log in real time and keeps a history so you can view the old messages. Follow the steps given below to view the device logs.

1.  If you haven't already [set up a remote session](about:blank#RemoteControlforAndroidDevices-Createaremotesession), set it up now.
2.  Click **Logcat**.

You can now see the log for the device. 

<img src="../../../../image/remote_session_05.png" style="border:5px solid black" >

## View the device screen via the remote session

To troubleshoot a device, it can be helpful to view the device's screen so you can monitor how the device owner is using it and then take actions yourself, such as opening applications and configuring settings. To view the screen, take the following steps: 

1.  If you haven't already [set up a remote session](about:blank#RemoteControlforAndroidDevices-Createaremotesession), set it up now.
2.  Click **Screen Share > Start**.  
    <img src="../../../../image/remote_session_06.png" style="border:5px solid black" >
3.  A message is sent to the device asking the device owner to share the screen with Entgra IoT Server. After the device owner accepts this message, you can view the device's screen. 
    
    <img src="../../../../image/remote_session_08.png" style="border:5px solid black" >
4.  Click **Stop** to stop viewing the screen of the device.

    

## View and interact with the device via the remote session

Viewing the screen of the device alone does not help you to solve the issue. You need to be able to carry out actions on the shared screen to successfully troubleshoot the device. Follow the steps given below to try it out:

**Method 1 : Using Accessibility Service (For non rooted devices)**
{{< hint info >}}
   <strong>Pre-requisites</strong>
   <br>
   <ul style="list-style-type:disc;">
       <li>Android OS version should be higher than Android 7.0 Nougat (API 24)</li>
   </ul>
   {{< /   hint >}}
1.  To start giving touch inputs using the mouse, device owner have to enable **Entgra IoT Remote Touch** accessibility service from automatically popped up settings page on the device. (If it's already enabled "Entgra IoT Remote Touch" accessibility service at enrollment, this step is not necessary) 

    <img  width='40%' height='30%'  src="../../../../image/remote_session_07.png" style="border:5px solid black" >

**Method 2 : Using System APIs**

1.  Enable the Android System Service Applications. 
    1.  Build the system service application.
        1.  [Download the source code](https://github.com/wso2/cdmf-agent-android.git).
        2.  The system service app can not be built via the usual Android developer Software Development Kit (SDK), as it requires access to [developer restricted APIs](about:blank#RemoteControlforAndroidDevices-Operationssupportedviathesystemserviceapplication). Therefore, you need to replace the existing `android.jar` file that is under the `<SDK_LOCATION>/platforms/android-<COMPILE_SDK_VERSION>` directory with the explicitly built `android.jar` file that has access to the restricted APIs. You can get the new `jar` file using one of the following options:   

            *   [Download the Android Open Source Project (AOSP)](https://source.android.com/compatibility/cts/downloads.html) and build the source code to get the `jar` file for the required SDK level.

            *   Use a pre-built jar file from a third party developer. You can find it [here](https://github.com/anggrayudi/android-hidden-api).
                 Make sure to use the jar file that matches the `compileSdkVersion` of the WSO2 Android agent. The current `compileSdkVersion` is 25.

        3.  Open the system service application source code via Android Studio and clean build it as a usual Android application.
    
    2.  Sign the application via the device firmware signing key. If you don’t have access to the firmware signing key, you have to get the system application signed via your device vendor. 

        For more information of signing the system service, see [Signing Your Applications](http://developer.android.com/intl/zh-cn/tools/publishing/app-signing.html).

    3.  Install the system service application by following any of the methods given below:

        *   If you have your own firmware, the system service application is available out of the box with your firmware distribution.

            1.  Copy the signed system service APK file to the `/system/priv-apps` directory of the device. 

            2.  When the device boots or restarts for the first time, it automatically installs the application as a system application.

        *   Install the system service application externally via an Android Debug Bridge (adb) command.

            For more information on how this takes place on Entgra IoTS, see [Configuring the service application](https://entgra.atlassian.net/wiki/spaces/IoTS360/pages/352790543/Device+Ownership+Application).

    4.  Enable the system service invocations through the WSO2  Android Agent application. 

        1.  Clone the `cdmf-agent-android` GIT repository. This is referred to as `<ANDROID_AGENT_SOURCE_CODE>` throughout this document.

            `https://github.com/wso2/cdmf-agent-android.git -b <ENTER_THE_VERSION>`

            Check the [Entgra IoT Server and Agent Compatibility](https://entgra.atlassian.net/wiki/spaces/IoTS360/pages/352781237/Entgra+IoT+Server+and+Agent+Compatibility) and find out what branch of this repo you need to clone.

        2.  Open the client folder that is in the `<ANDROID_AGENT_SOURCE_CODE>` via Android Studio.

        Make sure to sign the Android agent using the same device firmware signing key that was used to sign the System Service Application, else you run into security exceptions.

    1.  Navigate to the `Constants.java` class, which is in the `org.wso2.iot.agent.utils` package and configure the `SYSTEM_APP_ENABLED` field as follows:

        ` public static final boolean SYSTEM_APP_ENABLED = true;`

    2.  Rebuild the Android agent application.

    6.  Install the Android agent you just built to your mobile device.  
        You need to copy the APK to you device and install it. For more information on installing the Android agent, see [Registering an Android device](https://entgra.atlassian.net/wiki/spaces/IoTS360/pages/352781661/Android+Device#AndroidDevice-install). Follow the steps from step 6 onwards. 

2.  Restart or start Entgra IoT Server's core profile, which corresponds to the WSO2 Connected Device Management Framework (WSO2 CDMF) profile.

3.  Access the device management console: `https://<IOTS_HTTPS_HOST>:9443/devicemgt`  
    For example: [`https://localhost:9443/devicemgt`](https://localhost:9443/devicemgt)

4.  Sign in as an administrator. By default, the username is `admin` and the password is `admin`.

5.  Under **DEVICES**, click **View**.  
    <img src="../../../../image/remote_session_01.png" style="border:5px solid black" >
    
    You are navigated to a page that lists out all the devices that are enrolled with Entgra IoT Server because you have administrator privileges. If you do not have administrator privileges, you only the see the devices that you enrolled.

6.  Click on the device you want to start a remote session.

7.  Click the **Remote Session** tab.

8.  Click **Connect to Device** to start a remote session with the device. 

9.  Click **Screen Share > Start**.   
    <img src="../../../../image/remote_session_08.png" style="border:5px solid black" >

10.  Click on the applications you want to open or the configurations you want to enable using the mouse.

11.  Click **Stop** to stop viewing the screen of the device.
