---
bookCollapseSection: true
weight: 4
---

# Enrollment Guide

The Enrollment guide is split into 4 main sections, taking you through different types of enrollments available for Android, iOs, Mac OS and Windows devices.

The required Android Platform Configurations are given under the Android Enrollments. A guide to installing the Entgra Android Device Management app, either from the IoT server or from the Playstore, is also available under this section. 
Covered next are the different enrollment types available for Android devices with step-by-step procedures. It is possible to enroll Android devices as the Work Profile type, both with and without the QR code. Same goes for Fully Managed and Legacy types of enrollments, while for dedicated devices, enrollment with Entgra Agent using the QR code is also available. Also covered within this section is how to enroll an Android device as a Google Work Profile. 

A section describing how an iOS device can be enrolled to the IoT server, is given next for BYOD enrollment without the Entgra Agent. Apple server configurations are specified in detail, along with the configurations for Apple’s Device Enrollment Program (DEP). Procedures for enrolling iOs devices with and without the Entgra Agent, and for DEP is given in this section.

Process for enrolling a Mac OS device manually is available for Mac users. 

For Windows device enrollments, the additional server configurations are given followed up by the  procedure for enrolling a windows device.
