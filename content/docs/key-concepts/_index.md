---
bookCollapseSection: true
weight: 4
---

# Key Concepts

Let's take a look at some concepts and terminology that you need to know in order to follow the use cases.
